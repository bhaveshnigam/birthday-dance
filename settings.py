# Put your facebook profile ID here
# Currently using profile ID for bhavesh.nigam
# One can use http://findmyfacebookid.com/ to get the numeric Facebook ID.
FACEBOOK_PROFILE_ID = '1504662739'


# Facebook oauth access token is stored here.
# This is a long validity token for user bhavesh.nigam (Last Refreshed on 19th July)
# This has validity of 60 days.
# To refresh token goto: https://developers.facebook.com/tools/explorer
OAUTH_ACCESS_TOKEN = 'CAAEisJW29eQBAKmGenAGtYAGwF9XCAuK6kYkDZBrICQEeS0bziuCObJFC6W30UIfL1bdcYheak9txLsBFuJMSZBl8EeHE48xLtbNad2f08pYKdvZBJUZCijAYoZAO6qCkvWWly6xTkPc9anNbVYJfJvEPoxtAfV7GtadZBJPu9ZCcL1vn53PM0x'


# Parameters of your app and the id of the profile you want to mess with.
# Currently using Test App, created by bhavesh.nigam
FACEBOOK_APP_ID     = '319616798094820'
FACEBOOK_APP_SECRET = 'a8b1be0c5571cc5c135f8106e3cb74c1'


# Key words which uniquely identify a post a wish
WISH_KEYWORDS = [
    'birthday',
    "b'day",
    'bday',
    'bdy',
    'janamdin',
]


# Store a global Facebook Graph API instance.
FB_GRAPH_API = None


# Default Fetch limit
FETCH_LIMIT = 500

# Reply messages
REPLIES = [
    "Thanks for this... :):P",
    "Really Appreciated..! :)",
    "Yayeee... :):)",
    "Sweet..! :):D",
    "Thanks!. :)B-)",
    "Thank you for the wishes"
]


# Repeat replies
RE_REPLY = (
    "Thanks... I am not sure what to reply, but I'll notify Bhavesh regarding "
    "the repeat post... u rock...!")



# Tail Responder. Adds this to the tail of the reply message.
# Use it for signature.
TAIL_MESSAGE = "--Let's Dance \o/"


# Set USE_TAIL_MESSAGE to True if you want to add Tail Message specified in
# TAIL_MESSAGE.
USE_TAIL_MESSAGE = True


# Your Birthday Month.
BIRTHDAY_MONTH = 9 # INT


# Your Birthday Date.
BIRTHDAY_DATE = 27 # INT


# The visiblity scope required for access of the timeline and other user
# accessiblity from the API.
FB_SCOPE = (
    'user_actions.books,user_actions.music,'
    'user_actions.news,user_actions.video,user_checkins,'
    'user_education_history,user_events,'
    'user_games_activity,user_groups,user_hometown,'
    'user_notes,user_online_presence,'
    'user_photo_video_tags,user_photos,user_questions,'
    'user_relationship_details,user_relationships,'
    'user_religion_politics,user_status,'
    'user_subscriptions,user_videos,friends_about_me,'
    'friends_actions.books,friends_actions.music,'
    'friends_actions.news,friends_activities,'
    'friends_birthday,friends_checkins,'
    'friends_education_history,friends_events,'
    'friends_games_activity,friends_groups,'
    'friends_hometown,friends_interests,friends_likes,'
    'friends_location,friends_notes,'
    'friends_online_presence,friends_photo_video_tags,'
    'friends_questions,friends_relationship_details,'
    'friends_relationships,friends_religion_politics,'
    'friends_status,friends_subscriptions,friends_videos,'
    'friends_website,friends_work_history,create_event,'
    'create_note,export_stream,manage_friendlists,'
    'manage_notifications,manage_pages,photo_upload,'
    'publish_actions,publish_checkins,publish_stream,'
    'read_friendlists,read_insights,read_mailbox,'
    'read_page_mailboxes,read_requests,read_stream,'
    'rsvp_event,share_item,sms,status_update,'
    'video_upload,xmpp_login'
)
