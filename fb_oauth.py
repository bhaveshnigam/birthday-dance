#!/usr/bin/python
# coding: utf-8

import facebook
import urllib
import urlparse
import subprocess
import warnings
import settings
import logging

# Hide deprecation warnings. The facebook module isn't that up-to-date (facebook.GraphAPIError).
warnings.filterwarnings('ignore', category=DeprecationWarning)


def RefreshOAuthCredentials():
  # Trying to get an access token. Very awkward.
  oauth_args = dict(client_id = settings.FACEBOOK_APP_ID,
                    client_secret = settings.FACEBOOK_APP_SECRET,
                    grant_type = 'client_credentials',
                    scope=settings.FB_SCOPE
  )
  oauth_curl_cmd = ['curl',
                    'https://graph.facebook.com/oauth/access_token?' + urllib.urlencode(oauth_args)]

  oauth_response = subprocess.Popen(oauth_curl_cmd,
                                    stdout = subprocess.PIPE,
                                    stderr = subprocess.PIPE).communicate()[0]

  try:
      settings.OAUTH_ACCESS_TOKEN = urlparse.parse_qs(str(oauth_response))['access_token'][0]
  except KeyError:
      print ('Unable to grab an access token!')
      exit()


# # Try to post something on the wall.
# try:
#     fb_response = facebook_graph.put_wall_post(
#         'Hello from Python',
#         profile_id = settings.FACEBOOK_PROFILE_ID)
#     print fb_response
# except facebook.GraphAPIError as e:
#     print 'Something went wrong:', e.type, e.message

if __name__ == "__main__":
  RefreshOAuthCredentials()