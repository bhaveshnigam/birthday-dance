#!/usr/bin/python

import os
import re
import csv
import random
import settings
import fb_oauth
import facebook
import datetime
import calendar
import task_schedular


CSV_HEADERS = [
    'Message',
    'From',
    'Date Time Created on',
    'Reply Posted',
]

# The CSV file to save the birthday logs
CSV_FILENAME = 'BirthdayDanceLogs.csv'


def GetTimestampFromDate(date):
  """Returns a epoch timestamp for the provided date.

    Args:
      date: A datetime.datetime object to convert to timestamp.
  """
  return calendar.timegm(date.timetuple())


# The timestamp the feed was last fetched at.
# Default is set to day after the last year's birthday
# For Eg: If your last birthday was 27th Sept 2013, this will be set to
# 28th September 2013
settings.FEED_LAST_FETCHED_AT = GetTimestampFromDate(
    datetime.datetime(month=settings.BIRTHDAY_MONTH,
                      day=settings.BIRTHDAY_DATE + 1,
                      year=datetime.datetime.now().year - 1)
)


def UpdateLastFetched():
  """Updates the last fetch timestamp the current timestamp."""
  settings.FEED_LAST_FETCHED_AT = GetTimestampFromDate(datetime.datetime.now())


def LogToCSV(post, reply_msg):
  """Saves the data related to post in a CSV file for future reference.

  Args:
    post: The post dictionary object having all the essential information
        related to the post
    reply_msg: The reply_msg Which was posted as reply to the post.
    """
  data = [post.get('message', ''),
          post.get('from', {}).get('name'),
          post.get('created_time'),
          reply_msg]
  if os.path.exists(os.path.join(os.getcwd(), CSV_FILENAME)):
    # The CSV file already exists, append data to it.
     with open(os.path.join(os.getcwd(), CSV_FILENAME), 'a+') as csvfile:
       csv_writer = csv.writer(csvfile, dialect='excel')
       csv_writer.writerow(data)
  else:
    with open(os.path.join(os.getcwd(), CSV_FILENAME), 'wb') as csvfile:
      csv_writer = csv.writer(csvfile, dialect='excel')
      csv_writer.writerows([CSV_HEADERS, data])


def FetchFeeds():
  """Check for posts made after the time the feed was last fetched on.

    Returns:
      The Fetched feed_data from facebook timeline.
  """
  if not settings.FB_GRAPH_API:
    InitializeGraphAPI()
  try:
    feed_data = settings.FB_GRAPH_API.get_connections(
        settings.FACEBOOK_PROFILE_ID, 'feed',
        since=settings.FEED_LAST_FETCHED_AT,
        limit=settings.FETCH_LIMIT)['data']
    UpdateLastFetched()
  except KeyError:
    feed_data = []
  return feed_data


def ProcessFeeds(feeds):
  """Process the fetched feeds to look for wishes.

    Args:
      feeds: The feeds fetched from users timeline.
  """
  for post in feeds:
    if post.get('type', '') == 'status' and IsPostAWish(post):
      print '*** Posting Reply to "%(message)s" from %(sender)s*** ' % {
          'message': post.get('message', ''),
          'sender': post.get('from', {}).get('name')
      }
      LikeAndReply(post)


def IsPostAWish(post):
  """Checks whether the provided post is a wish.

      Predicts the provided post is a wish if it contains any of the
      words/phrases specified in settings.WISH_KEYWORDS.

      Args:
        post: The post dictionary object having all the essential information
            related to the post

      Returns:
        True if the post is considered as a wish else False.
  """
  post_message = re.findall(r'[\w]+', post.get('message', ''))
  from_id = post.get('from', {}).get('id', '')
  for word in post_message:
    if (word.lower() in settings.WISH_KEYWORDS and
        not from_id == settings.FACEBOOK_PROFILE_ID):
      return True
  return False


def LikeAndReply(post):
  """Like the provided post and post a reply.

    Posts a random message from one of the messages from settings.REPLIES

    Args:
      post: The post dictionary object having all the essential information
          related to the post.
  """
  reply_msg = random.choice(settings.REPLIES)
  if settings.USE_TAIL_MESSAGE:
    reply_msg = '\n'.join([random.choice(settings.REPLIES),
                           settings.TAIL_MESSAGE])
  for comment in post.get('comments', {}).get('data', []):
    comment_msg = comment.get('message')
    comment_msg = comment_msg.replace(settings.TAIL_MESSAGE, '')
    if comment_msg in settings.REPLIES:
      reply_msg = settings.RE_REPLY
      break
  try:
    settings.FB_GRAPH_API.put_like(post.get('id'))
    settings.FB_GRAPH_API.put_comment(post.get('id'), reply_msg)
    LogToCSV(post, reply_msg)
  except facebook.GraphAPIError:
    print ('****Posting a reply comment failed due to some reason, '
           'retrying in some time ****')



def InitializeGraphAPI():
  """Initialize the Facebook Graph API as per the settings defined and store it
     in a instance in settings.

    Raises:
      ValueError on not being able to properly set up the Graph API.
  """
  if not settings.OAUTH_ACCESS_TOKEN:
    # This does not fetch the proper User Access Token, so does not provide
    # full visibility of all the posts.
    fb_oauth.RefreshOAuthCredentials()
  if settings.OAUTH_ACCESS_TOKEN and not settings.FB_GRAPH_API:
    settings.FB_GRAPH_API = facebook.GraphAPI(settings.OAUTH_ACCESS_TOKEN)
    extended_token = settings.FB_GRAPH_API.extend_access_token(
        settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET).get('access_token')
    if extended_token:
      settings.OAUTH_ACCESS_TOKEN = extended_token
      settings.FB_GRAPH_API = facebook.GraphAPI(settings.OAUTH_ACCESS_TOKEN)
  else:
    raise ValueError('Unable to authenticate with Facebook Graph API')


def StartDance():
  """Initiates the process to check Facebook feeds and respond automatically."""
  if not settings.FB_GRAPH_API:
    InitializeGraphAPI()
  feeds = FetchFeeds()
  ProcessFeeds(feeds)


class ScheduleTheDance(task_schedular.TaskThread):
  """Class to check feeds at predefined rate by settings.REFRESH_INTERVAL."""

  def task(self):
    today = datetime.date.today()
    if not (today.day == settings.BIRTHDAY_DATE and
            today.month == settings.BIRTHDAY_MONTH):
      self.shutdown()
      print 'Today: %(today)s is not your birthday. Exiting' % {'today': today}
      exit()
    StartDance()


if __name__ == "__main__":
  dance_thread = ScheduleTheDance()
  dance_thread.start()
